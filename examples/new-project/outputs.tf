# [terraform-project]/outputs.tf

output "gcp_cloud_dns_managed_zone" {
  description = "Example Cloud DNS Managed Zone"
  value       = module.gcp_cloud_dns_managed_zone
}
