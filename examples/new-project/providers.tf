# [terraform-project]/providers.tf

# Define the Google Provider
provider "google" {
  project = var.gcp_project
}
