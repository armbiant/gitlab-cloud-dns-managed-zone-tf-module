# outputs.tf

output "root_zone" {
  description = "Cloud DNS Managed Zone configuration"
  value = {
    "id" : google_dns_managed_zone.root_zone.id
    "name" : google_dns_managed_zone.root_zone.name
    "dns_name" : google_dns_managed_zone.root_zone.dns_name
    "description" : google_dns_managed_zone.root_zone.description
    "dnssec" : google_dns_managed_zone.root_zone.dnssec_config
    "project" : google_dns_managed_zone.root_zone.project
    "name_servers" : google_dns_managed_zone.root_zone.name_servers
  }
}

output "record_a_root" {
  description = "A map with the root A record configuration."
  value = {
    "enabled" :length(var.root_a_record) > 0  ? "true" : "false"
    "name" : length(var.root_a_record) > 0  ? google_dns_record_set.record_a_root[0].name : null
    "id" : length(var.root_a_record) > 0  ? google_dns_record_set.record_a_root[0].id : null
    "records" : length(var.root_a_record) > 0  ? toset(google_dns_record_set.record_a_root[0].rrdatas) : null
    "ttl" : length(var.root_a_record) > 0  ? google_dns_record_set.record_a_root[0].ttl : null
    "type" : length(var.root_a_record) > 0  ? google_dns_record_set.record_a_root[0].type : null
  }
}

output "records_a" {
  description = "A map with record name and IP address value."
  value = toset([
    for key, value in google_dns_record_set.record_a : {
      (key) : {
        "name" : value.name
        "id" : value.id
        "records" : value.rrdatas
        "ttl" : value.ttl
        "type" : value.type
      }
    }
  ])
}

output "records_cname" {
  description = "A map with record name and CNAME value."
  value = toset([
    for key, value in google_dns_record_set.record_cname : {
      (key) : {
        "name" : value.name
        "id" : value.id
        "records" : value.rrdatas
        "ttl" : value.ttl
        "type" : value.type
      }
    }
  ])
}

output "record_mx_root" {
  description = "A list with MX values for top-level domain."
  value = {
    "name" : length(var.root_mx_records) > 0 ? google_dns_record_set.record_mx_root[0].name : null
    "id" : length(var.root_mx_records) > 0 ? google_dns_record_set.record_mx_root[0].id : null
    "records" : length(var.root_mx_records) > 0 ? toset(google_dns_record_set.record_mx_root[0].rrdatas) : null
    "ttl" : length(var.root_mx_records) > 0 ? google_dns_record_set.record_mx_root[0].ttl : null
    "type" : length(var.root_mx_records) > 0 ? google_dns_record_set.record_mx_root[0].type : null
  }
}

output "record_mx_subdomain" {
  description = "A map with MX record name and MX value list for subdomains"
  value = toset([
    for key, value in google_dns_record_set.record_mx_subdomain : {
      (key) : {
        "name" : value.name
        "id" : value.id
        "records" : value.rrdatas
        "ttl" : value.ttl
        "type" : value.type
      }
    }
  ])
}

output "records_txt" {
  description = "A map with TXT record name and TXT value"
  value = toset([
    for key, value in google_dns_record_set.record_txt : {
      (key) : {
        "name" : value.name
        "id" : value.id
        "records" : value.rrdatas
        "ttl" : value.ttl
        "type" : value.type
      }
    }
  ])
}

output "subzone" {
  description = "A map with the subdomain name and a list of name servers that host the subzone configuration."
  value = toset([
    for key, value in google_dns_record_set.subzone : {
      (key) : {
        "name" : value.name
        "id" : value.id
        "records" : value.rrdatas
        "ttl" : value.ttl
        "type" : value.type
      }
    }
  ])
}
